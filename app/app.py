from flask import Flask
from flask import jsonify
from flask import request
import aiml
import os
############################
#### Chat-Bot-T - Back
#### v1.0 - Julien Stroheker
############################

kernel = aiml.Kernel()

if os.path.isfile("bot_brain.brn"):
    kernel.bootstrap(brainFile = "bot_brain.brn")
else:
    kernel.bootstrap(learnFiles = "std-startup.xml", commands = "load aiml b")
    kernel.saveBrain("bot_brain.brn")

app = Flask(__name__)
        
@app.route('/')
def miaouw():
   return 'Miaouwwwww....'
      
@app.route('/request', methods=['POST'])
def requestjson():
    app.logger.debug("JSON received...")
    app.logger.debug(request.json)
    if request.json:
        value = request.json
        return jsonify(answer=kernel.respond(value.get("question")))
    else:
        return "Miaouww ... No JSON..."

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0', port=12345)
    
